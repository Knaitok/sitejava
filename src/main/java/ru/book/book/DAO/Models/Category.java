package ru.book.book.DAO.Models;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

//    @OneToMany(mappedBy="category", fetch=FetchType.EAGER)
//    private Collection<Product> products;
}
