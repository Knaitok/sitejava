package ru.book.book.DAO.Models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    private Double price;

//    @ManyToOne(optional=false, cascade=CascadeType.ALL)
//    private Category category;
}
