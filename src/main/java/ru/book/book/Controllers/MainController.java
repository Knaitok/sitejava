package ru.book.book.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
    public class MainController {

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("name", "Мы тут");
        return "index";
    }
    @GetMapping("/addProduct")
    public String addProduct(Model model) {
        return "addProduct";
    }
    @GetMapping("/basket")
    public String basket(Model model) {
        return "basket";
    }
    @GetMapping("/authorization")
    public String authorization(Model model) {
        return "authorization";
    }
}
